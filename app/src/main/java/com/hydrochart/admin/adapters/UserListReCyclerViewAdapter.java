package com.hydrochart.admin.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.hydrochart.admin.AdminLoginActivity;
import com.hydrochart.admin.ChatWithAdminActivity;
import com.hydrochart.admin.models.ChatUsers;
import com.hydrochart.admin.R;

import java.util.ArrayList;

public class UserListReCyclerViewAdapter extends RecyclerView.Adapter<UserListReCyclerViewAdapter.ViewHolder> {

    private ArrayList<ChatUsers> listData;

    // RecyclerView recyclerView;
    public UserListReCyclerViewAdapter(ArrayList<ChatUsers> listData) {
        this.listData = listData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.chat_users_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(listItem, listData);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ChatUsers myListData = listData.get(position);
        holder.textView.setText(myListData.getDisplayName());
//        holder.imageView.setImageResource(listdata[position].getImgId());
//        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(),"click on item: "+myListData.getTitle(),Toast.LENGTH_LONG).show();
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public TextView textView;
        public RelativeLayout relativeLayout;
        private ArrayList<ChatUsers> listData;

        public ViewHolder(View itemView, ArrayList<ChatUsers> listData) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.txtUserDisplayName);
            itemView.setOnClickListener(this);
            this.listData = listData;
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            ChatUsers mChatUser = listData.get(position);
            Context mContext = v.getContext();

            // Toast.makeText(mContext, mChatUser.getDisplayName(), Toast.LENGTH_SHORT).show();

            Intent mIntent = new Intent(mContext, ChatWithAdminActivity.class);
            mIntent.putExtra("PREVIOUS_ACTIVITY", AdminLoginActivity.class.getSimpleName());
            mIntent.putExtra("USER_DISPLAY_NAME", mChatUser.getDisplayName());
            mIntent.putExtra("CHAT_WITH_USER_ID", mChatUser.getUserName());
            mContext.startActivity(mIntent);
        }
    }
}

