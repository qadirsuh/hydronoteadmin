package com.hydrochart.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DashboardActivity extends AppCompatActivity {

    private Button btnChatWithUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        btnChatWithUsers = findViewById(R.id.btnChatWithUsers);
        btnChatWithUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(DashboardActivity.this, ChatUsersListActivity.class);
                mIntent.putExtra("PREVIOUS_ACTIVITY", AdminLoginActivity.class.getSimpleName());
                startActivity(mIntent);
            }
        });

    }
}