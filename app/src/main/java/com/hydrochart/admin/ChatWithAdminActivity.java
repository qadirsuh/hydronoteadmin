package com.hydrochart.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hydrochart.admin.adapters.ChatReCyclerViewAdapter;
import com.hydrochart.admin.models.ChatWithAdmin;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChatWithAdminActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ChatReCyclerViewAdapter adapter;

    private TextView username;
    private EditText edMessageInput;
    private ImageButton btnSend;
    private ImageButton btnAttach;

    private ArrayList<ChatWithAdmin> chatItems = new ArrayList<>();

    private ProgressDialog mProgressDialog;

    private String userIdToChatWith;
    private Intent mIntent;

    private int FILE_PICK_REQUEST_CODE = 100;
    private Uri filePathToUpload;
    private String fileSuffixName;

    //Firebase storage
    private FirebaseStorage mFirebaseStorage;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_with_admin);

        mIntent = getIntent();

        mProgressDialog = Utils.progressDialog("Loading...", this);

        username = findViewById(R.id.username);
        edMessageInput = findViewById(R.id.edMessageInput);
        btnSend = findViewById(R.id.btnSend);
        btnAttach = findViewById(R.id.btnAttach);

        mFirebaseStorage = FirebaseStorage.getInstance();
        storageReference = mFirebaseStorage.getReference();

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

        if (mIntent.getStringExtra("PREVIOUS_ACTIVITY").equalsIgnoreCase(AdminLoginActivity.class.getSimpleName())) {
            userIdToChatWith = mIntent.getStringExtra("CHAT_WITH_USER_ID");
            Log.e("userIdToChatWith", userIdToChatWith);
            String userDisplayName = mIntent.getStringExtra("USER_DISPLAY_NAME");
            username.setText("Chat with \"" + userDisplayName + "\"");
        } else {
            userIdToChatWith = Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME);
        }

        Log.e("userIdToChatWith", userIdToChatWith);

        adapter = new ChatReCyclerViewAdapter(chatItems, userIdToChatWith);
        recyclerView.setAdapter(adapter);

        // Fetch messages from firebase database
        getAllChatDbMessage();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = edMessageInput.getText().toString().trim();
                if (!message.isEmpty()) {

                    Map<String, Object> messageData = new HashMap<>();
                    messageData.put("createdAt", System.currentTimeMillis());
                    messageData.put("message", message);
                    /*if (Utils.getSharedPref(Utils.KEY_APP_TYPE).equalsIgnoreCase(Utils.KEY_ADMIN_APP)) {
                        messageData.put("isAdmin", true);
                    } else {
                        messageData.put("isAdmin", false);
                    }*/
                    messageData.put("createdByUser", Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME));
                    addMessageInDb(messageData);
                }
            }
        });


        btnAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ChatWithAdminActivity.this, FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowImages(true)
                        .enableImageCapture(true)
                        .setSingleChoiceMode(true)
                        .setSkipZeroSizeFiles(true)
                        .build());
                startActivityForResult(intent, FILE_PICK_REQUEST_CODE);

            }
        });

    }

    private void addMessageInDb(final Map<String, Object> messageData) {

        edMessageInput.setText("");
        btnSend.setEnabled(false);
        btnSend.setAlpha(0.5f);


        final DatabaseReference chatWithAdminPath = App.sInstance.getDbRoot().child("chatWithAdmin/" + userIdToChatWith + "");
        DatabaseReference messageId = chatWithAdminPath.push();
        messageId.setValue(messageData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (!task.isSuccessful()) {
                    edMessageInput.setText(messageData.get("message").toString());
                    Toast.makeText(ChatWithAdminActivity.this, "Sending failed! Please try again later.", Toast.LENGTH_LONG).show();
                }

                btnSend.setEnabled(true);
                btnSend.setAlpha(1f);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FILE_PICK_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                ArrayList<MediaFile> files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);

                for (MediaFile file : files) {

                    fileSuffixName = file.getName();
                    filePathToUpload = file.getUri();

                    Log.e("getName", file.getName());
                    Log.e("getMimeType", file.getMimeType());
                    Log.e("getPath", file.getPath());
                    Log.e("getSize", file.getSize()+"");
                    Log.e("getBucketId", file.getBucketId()+"");
                    Log.e("getBucketName", file.getBucketName()+"");
                    Log.e("getDuration", file.getDuration()+"");
                    Log.e("getId", file.getId()+"");
                    Log.e("getThumbnail", file.getThumbnail()+"");
                    Log.e("getUri", file.getUri()+"");
                    Log.e("getWidth", file.getWidth()+"");
                    Log.e("getHeight", file.getHeight()+"");
                    Log.e("getMediaType", file.getMediaType()+"");

                    Map<String, Object> attachmentData = new HashMap<>();
                    attachmentData.put("createdAt", System.currentTimeMillis());
                    attachmentData.put("createdByUser", "admin");
                    attachmentData.put("message", fileSuffixName);
                    attachmentData.put("attachment", "true");
                    attachmentData.put("filePath", filePathToUpload.toString());
                    uploadFileDb(attachmentData);
                }
            }
        }
    }

    private void uploadFileDb(final Map<String, Object> data) {

        mProgressDialog = Utils.progressDialog("Uploading...", this);
        mProgressDialog.show();


        Log.e("fileSuffixName", fileSuffixName);

        final StorageReference ref = storageReference.child("images/" + fileSuffixName);

        ref.putFile(Uri.parse(data.get("filePath").toString()))
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // send Request after uploading the file
                        Log.e("downloadUrlMeta", taskSnapshot.getMetadata().getPath());
                        //Log.e("getDownloadUrl", taskSnapshot.getStorage().getDownloadUrl().getResult().get);

                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                // Log.e(TAG, "onSuccess: uri= "+ uri.toString());

                                Log.e("getDownloadUrlURI", uri.toString());

                                data.put("downloadUrl", uri.toString());
                                addAttachmentInDb(data);
                            }
                        });


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(ChatWithAdminActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                .getTotalByteCount());
                        mProgressDialog.setMessage("Uploading... " + (int) progress + "%");
                    }
                });
    }

    private void addAttachmentInDb(final Map<String, Object> messageData) {

        btnAttach.setEnabled(false);
        btnAttach.setAlpha(0.5f);

        final DatabaseReference chatWithAdminPath = FirebaseDatabase.getInstance().getReference().child("chatWithAdmin/" + userIdToChatWith + "");
        Log.e("chatWithAdminPath", chatWithAdminPath.getPath().toString());
        DatabaseReference messageId = chatWithAdminPath.push();
        messageId.setValue(messageData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (!task.isSuccessful()) {
                    edMessageInput.setText(messageData.get("message").toString());
                    Toast.makeText(ChatWithAdminActivity.this, "Sending failed! Please try again later.", Toast.LENGTH_LONG).show();
                }

                btnAttach.setEnabled(true);
                btnAttach.setAlpha(1f);
            }
        });
    }

    private Query uidRef;
    private ValueEventListener valueEventListener;

    private void getAllChatDbMessage() {

        mProgressDialog.show();

        final DatabaseReference adminChatPath = App.sInstance.getDbRoot().child("chatWithAdmin/" + userIdToChatWith + "");
        Log.e("adminChatPath", adminChatPath.getPath().toString());
        uidRef = adminChatPath.orderByKey();
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();
                chatItems.clear();

                try {

                    if (dataSnapshot.getChildrenCount() == 0) {
                        Toast.makeText(ChatWithAdminActivity.this, "Chat is empty!", Toast.LENGTH_SHORT).show();
                    }

                    Log.e("dataSnapshot count", dataSnapshot.getChildrenCount() + "");

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        ChatWithAdmin mChatObj = postSnapshot.getValue(ChatWithAdmin.class);
                        mChatObj.setKey(postSnapshot.getKey());
                        Log.e("message", mChatObj.getMessage());
                        chatItems.add(mChatObj);
                    }

                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }

    private void addUserEntryInChatListForAdmin(Map<String, Object> userEntryData) {
        final DatabaseReference chatWithAdminPath = App.sInstance.getDbRoot().child("chatWithAdmin/chatListUser/" + userIdToChatWith + "");
        chatWithAdminPath.setValue(userEntryData);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uidRef.removeEventListener(valueEventListener);
    }
}
