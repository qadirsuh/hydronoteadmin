/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hydrochart.admin;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.provider.Settings;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static final String PREFS_NAME = "PREF";
    public static final String KEY_APP_TYPE = "APP_TYPE";
    public static final String KEY_ADMIN_APP = "ADMIN_APP";
    public static final String KEY_FARMER_APP = "USER_APP";
    public static final String KEY_LOGIN_USER_NAME = "LOGIN_USER_NAME";
    public static final String KEY_LOGIN_USER_DISPLAY_NAME = "LOGIN_USER_DISPLAY_NAME";
    public static final String KEY_LOGIN_PASSWORD = "LOGIN_PASSWORD";

    static String getTodayDate() {
        Date cDate = new Date();
        String fDate = new SimpleDateFormat("dd-MM-yyyy").format(cDate);
        return fDate;
    }

    static String getUserId() {
        Context mContext = App.sInstance.getApplicationContext();
        String deviceId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    static void saveSharedPref(String KEY, String value) {

        Context mContext = App.sInstance.getApplicationContext();
        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY, value);
        editor.apply();
    }


    public static String getSharedPref(String KEY) {

        Context mContext = App.sInstance.getApplicationContext();
        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String key = sharedPref.getString(KEY, "null");
        return key;
    }

    public static ProgressDialog progressDialog(String title, Context mContext) {

        try {

            ProgressDialog dialog = new ProgressDialog(mContext); // this = YourActivity
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage(title);
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.show();

            return dialog;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void showInfoAlert(Context mContext, String title, String message, String button_yes, String button_no, final Runnable runnablePositive, final Runnable runnableNegative) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon(R.drawable.ic_alert_success);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(button_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (runnablePositive != null) {
                    runnablePositive.run();
                }
            }
        });

        alertDialog.setNegativeButton(button_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (runnableNegative != null) {
                    runnableNegative.run();
                }
            }
        });

        alertDialog.show();
    }

}
