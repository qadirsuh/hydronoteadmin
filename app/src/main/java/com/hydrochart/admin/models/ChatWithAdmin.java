package com.hydrochart.admin.models;

import java.io.Serializable;

public class ChatWithAdmin implements Serializable {

    private long createdAt;
    private String createdByUser;
    private String downloadUrl;
    private String filePath;
    private String attachment;
    private String message;
    private String key;

    public ChatWithAdmin() {
    }

    public ChatWithAdmin(long createdAt, String createdByUser, String downloadUrl, String filePath, String attachment, String message, String key) {
        this.createdAt = createdAt;
        this.createdByUser = createdByUser;
        this.downloadUrl = downloadUrl;
        this.filePath = filePath;
        this.attachment = attachment;
        this.message = message;
        this.key = key;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
